import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AlertService } from 'src/app/services/alert/alert.service';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-add-images',
  templateUrl: './add-images.component.html',
  styleUrls: ['./add-images.component.scss']
})
export class AddImagesComponent implements OnInit {

  file: any = null;
  properties: any[] = [];

  formData = new FormData();
  fileName: string | ArrayBuffer | null = "https://banksiafdn.com/wp-content/uploads/2019/10/placeholde-image.jpg"
  features: any;
  type: any;
  id: any = 1;

  constructor(
    private alert: AlertService,
    private api: ApiService
  ) { }

  ngOnInit(): void {
    this.getProducts();
  }

  onFileChange(event: any) {
    this.file = event.target.files[0];
    this.fileName = this.file?.name;
    // this.formData.append('banner', "arstu");

    this.formData.append('file', this.file, this.fileName as string);
    const reader = new FileReader();
    reader.onload = e => this.fileName = reader.result;
    reader.readAsDataURL(this.file);
  }

  formSubmit(formRef: NgForm) {

    if (formRef.form.status === 'INVALID' || !this.file) {
      this.alert.show({
        title: "Required fields",
        message: "Please fill all required fields",
        icon: 'warning'
      });
      return;
    }
    const values = formRef.form.value;

    this.api.post(`/api/properties/add-image/${values?.propertyId}`, this.formData).subscribe((res: any) => {
      this.fileName = "https://banksiafdn.com/wp-content/uploads/2019/10/placeholde-image.jpg";
      this.file = null;
      formRef.resetForm();
      this.formData.delete('name');
      this.formData.delete('address');
      this.formData.delete('price');
      this.formData.delete('type');
      this.formData.delete('features');

      this.alert.show({
        title: res?.success ? "Success" : "Error",
        message: res?.message,
        icon: res?.success ? 'success' : 'error'
      });
    })

  }

  getProducts() {
    this.api.get(`/api/properties?pageNumber=0&recordsPerPage=100000`)
      .subscribe((res: any) => {
        this.properties = res?.content;
      })
  }
}
