import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/storage/session.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  routes = [
    {
      path: '/admin',
      label: 'Home',
      icon: 'home'
    },
    {
      path: 'product',
      label: 'Add Property',
      icon: 'styler'
    },
    {
      path: 'customer-list',
      label: 'Bookings',
      icon: 'group'
    },
    {
      path: 'add-image',
      label: 'Add Gallery to Property',
      icon: 'add_photo_alternate'
    },
    {
      path: 'feedback',
      label: 'Messages',
      icon: 'chat'
    }



  ]
  constructor(
    private session: SessionService,
    private route: Router
  ) { }

  ngOnInit(): void {
  }

  logout() {
    this.session.clear();
    this.route.navigate(['/sign']);
  }

}
