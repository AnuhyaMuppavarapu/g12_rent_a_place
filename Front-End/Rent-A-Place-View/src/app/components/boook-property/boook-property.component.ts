import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DashboardComponent } from 'src/app/pages/dashboard/dashboard.component';
import { AlertService } from 'src/app/services/alert/alert.service';
import { ApiService } from 'src/app/services/api.service';
import { SessionService } from 'src/app/services/storage/session.service';
import * as dayjs from 'dayjs';

@Component({
  selector: 'app-boook-property',
  templateUrl: './boook-property.component.html',
  styleUrls: ['./boook-property.component.scss']
})
export class BoookPropertyComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DashboardComponent>,
    private alert: AlertService,
    private api: ApiService,
    private session: SessionService,
    @Inject(MAT_DIALOG_DATA) public data: { propertyId: number, price: number }
  ) { }

  ngOnInit(): void {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  formSubmit(formRef: NgForm) {
    if (formRef.form.status === 'INVALID') {
      this.alert.show({
        title: "Required fields",
        message: "Please fill all required fields",
        icon: 'warning'
      });
      return;
    }

    const values = formRef.form.value;
    console.log(values);
    const d1 = dayjs(values?.checkOutDate);
    const d2 = dayjs(values?.checkInDate);
    const totalPrice = d1.diff(d2, 'day') * this.data.price;

    this.api.post(`/api/booking/save`, {
      propertyId: this.data.propertyId,
      bookedByUserId: this.session.getUsers()?.user?.userId,
      checkInDate: dayjs(values?.checkInDate).toISOString(),
      checkOutDate: dayjs(values?.checkOutDate).toISOString(),
      numberOfPersons: values?.numberOfPersons,
      totalPayment: totalPrice
    }).subscribe((res) => {
      this.alert.show({
        title: "Booking request sent",
        message: "Please wait for conformation",
        icon: 'success'
      });
      this.onNoClick()
    });

  }

}
