CREATE DATABASE  IF NOT EXISTS `hotel_management` ;
USE `hotel_management`;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` bigint NOT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `profile_image_url` varchar(255) DEFAULT NULL,
  `role` int DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- Table structure for table `booking`
--

DROP TABLE IF EXISTS `booking`;
CREATE TABLE `booking` (
  `id` bigint NOT NULL,
  `booked_on` datetime DEFAULT NULL,
  `check_in_date` datetime DEFAULT NULL,
  `check_out_date` datetime DEFAULT NULL,
  `is_confirmed` bit(1) DEFAULT NULL,
  `number_of_persons` bigint DEFAULT NULL,
  `total_payment` bigint DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  `properties_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkgseyy7t56x7lkjgu3wah5s3t` (`user_id`),
  KEY `FK3xtd90ryb4b7o1hnhk939a1ap` (`properties_id`),
  CONSTRAINT `FK3xtd90ryb4b7o1hnhk939a1ap` FOREIGN KEY (`properties_id`) REFERENCES `properties` (`id`),
  CONSTRAINT `FKkgseyy7t56x7lkjgu3wah5s3t` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id` bigint NOT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `property_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



--
-- Table structure for table `message_pool`
--

DROP TABLE IF EXISTS `message_pool`;

CREATE TABLE `message_pool` (
  `id` bigint NOT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `receiver` bigint DEFAULT NULL,
  `sender` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKmfcomyyrfynap78akq85u2f3y` (`receiver`),
  KEY `FKl3cavq9now3kkty1olgargrpb` (`sender`),
  CONSTRAINT `FKl3cavq9now3kkty1olgargrpb` FOREIGN KEY (`sender`) REFERENCES `user` (`user_id`),
  CONSTRAINT `FKmfcomyyrfynap78akq85u2f3y` FOREIGN KEY (`receiver`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `properties`
--

DROP TABLE IF EXISTS `properties`;

CREATE TABLE `properties` (
  `id` bigint NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `banner_url` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` bigint DEFAULT NULL,
  `type` int DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKquwk7bgwa55ofxqufldn2vb2x` (`user_id`),
  CONSTRAINT `FKquwk7bgwa55ofxqufldn2vb2x` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `property_feature`
--

DROP TABLE IF EXISTS `property_feature`;
CREATE TABLE `property_feature` (
  `id` bigint NOT NULL,
  `feature` int DEFAULT NULL,
  `property_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;






